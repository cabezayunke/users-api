module.exports = {
    "entitySample": {
        "__v": 0,
        "username": "username",
        "password": "password",
        "createdAt": "2017-08-04T17:06:31.274Z",
        "lastLogin": "2017-08-04T17:06:31.274Z",
        "_id": "5984a997f3bf94001cb97bec"
    },
    "updatedEntitySample": {
        "__v": 0,
        "username": "newusername",
        "password": "newpassword",
        "createdAt": "2017-08-04T17:06:31.274Z",
        "lastLogin": "2017-08-04T17:06:31.274Z",
        "_id": "5984a997f3bf94001cb97bec"
    },
    "invalidIds": ["invalidId", 3245345, null],
    "invalidPostData": [
        {
            "password": "missing name"
        },
        {
            "username": "missing password"
        },
        {
            "username": 235645267,
            "password": "invalid username"
        },
        {
            "username": "invalid password",
            "password": 32573568
        },
        {
            "username": "",
            "password": "empty username"
        },
        {
            "username": "empty password",
            "password": ""
        }
    ],
    "invalidPutData": [
        {
            "username": 235645267,
            "password": "invalid username"
        },
        {
            "username": "invalid password",
            "password": 32573568
        },
        {
            "username": "",
            "password": "empty username"
        },
        {
            "username": "empty password",
            "password": ""
        }
    ]
};