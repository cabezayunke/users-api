import UserModel from '../model/UserModel';
import ParamValidator from '../validator/ParamValidator';
import HttpStatus from 'http-status';
import Promise from 'bluebird';


/**
 * Users API controller
 * (will be used by the router)
 */
export default class UsersController {

    /**
     * Get entities
     *
     * @param req
     * @param res
     * @param next
     * @returns {Promise.<void>}
     */
    getUsers(req, res, next) {

        // set default pagination to get the first 10 items sorted by ID DESC
        let options = {
            limit:  req.query.hasOwnProperty('limit') ? parseInt(req.query.limit, 10) : 10,
            offset: req.query.hasOwnProperty('offset') ? parseInt(req.query.offset, 0) : 0,
            sort: {
                _id: req.query.hasOwnProperty('order') ? req.query.order : 'desc'
            }
        };

        // paginate already uses bluebird promises
        UserModel.paginate({}, options)
            .then((result) => {
                res.json(result);
            })
            .catch((err) => {
                next(err);
            });

    };

    /**
     * Get entity by ID
     *
     * @param req
     * @param res
     * @param next
     */
    getUserById(req, res, next) {

        let id = req.params.id;
        let isValid = ParamValidator.validateMongoId(id);
        console.log(isValid);

        if(isValid) {
            console.log(id);

            // bluebird promise to return entity by ID
            UserModel.findById(id)
            .then((data) => {
                if(data) {
                    res.json(data);
                }
                else {
                    res.sendStatus(HttpStatus.NOT_FOUND);
                }
            })
            .catch((err) => {
                console.warn(err);
                next(err);
            });

        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }
    };

    /**
     * Creates new entity
     *
     * @param req
     * @param res
     * @param next
     *
     */
    crateNewUser(req, res, next) {

        let isValid = ParamValidator.validateBody(
            req.body, // data
            [ // mandatory params
                "username", "password"
            ]
        );

        if(isValid.success) {
            let newEntity = new UserModel(req.body);
            // bluebird promise to return saved entity
            newEntity.save()
            .then(() => {
                console.log('New entity created');
                res.status(HttpStatus.CREATED).json(newEntity);
            })
            .catch((err) => {
                console.warn(err);
                next(err);
            });
        }
        else {
            res.status(HttpStatus.BAD_REQUEST).json(isValid.response);
        }

    };

    /**
     * Delete entity
     *
     * @param req
     * @param res
     * @param next
     */
    deleteUser(req, res, next) {

        let id = req.params.id;
        let isValid = ParamValidator.validateMongoId(id);
        console.log(isValid);

        if(isValid) {
            console.log(id);
            // bluebird promise to find blog post first
            UserModel.findById(id)
            .then((data) => {
                if(data) {
                    console.log('Entity to delete found');
                    // bluebird promise to delete blog post
                    return data.remove()
                }
                else {
                    res.sendStatus(HttpStatus.NOT_FOUND);
                }
            })
            .then(() => {
                console.log('Entity successfully deleted!');
                res.sendStatus(HttpStatus.OK);
            })
            .catch((err) => {
                console.warn(err);
                next(err);
            });

        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }
    }


    /**
     * Updates an entity
     *
     * @param req
     * @param res
     * @param next
     */
    updateUser(req, res, next) {

        let id = req.params.id;
        let isValidId = ParamValidator.validateMongoId(id);

        let isValidBody = ParamValidator.validateBody(
            req.body, // data
            [] // mandatory params
        );

        if(isValidId) {
            if(isValidBody.success) {
                // bluebird promise to return updated entity
                // we need the 'new' param to return the updated entity
                UserModel.findByIdAndUpdate(id, { $set: req.body }, {new: true})
                .then((data) => {
                    if(data) {
                        console.log('Entity updated');
                        res.status(HttpStatus.OK).json(data);
                    }
                    else {
                        res.sendStatus(HttpStatus.NOT_FOUND);
                    }
                })
                .catch((err) => {
                    console.warn(err);
                    next(err);
                });
            }
            else {
                res.status(HttpStatus.BAD_REQUEST).json(isValidBody.response);
            }
        }
        else {
            // validation error
            // to be consistent, we use the same error format here as the payload-validator
            res.status(HttpStatus.BAD_REQUEST).json(ParamValidator.getValidationError('_id'));
        }
    };

    /*
     * Custom controller methods here
     */

    /*
     * END of custom controller methods
     */
}