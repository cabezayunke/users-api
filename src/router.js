import { Router } from 'express';
import UsersController from './controller/UsersController';

const router = new Router();

/*
 * Users API routes
 */
const controller = new UsersController();
router.get('/users/:id', controller.getUserById);
router.get('/users', controller.getUsers);
router.post('/users', controller.crateNewUser);
router.delete('/users/:id', controller.deleteUser);
router.put('/users/:id', controller.updateUser);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

export default router;