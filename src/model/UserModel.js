import mongoose from 'mongoose';
import mongoosePaginate from 'mongoose-paginate';
import Promise from 'bluebird';

// make mongoose use bluebird
mongoose.Promise = Promise;

/**
 * Mongoose schema
 */
const schema = mongoose.Schema(
    {
        _id:  {
            type: mongoose.Schema.Types.ObjectId,
            index: true,
            required: true,
            auto: true
        },
        username: {
            type: String,
            required: true
        },
        password: {
            userId: String,
            name: String,
            photoUrl: String
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        lastLogin: {
            type: Date,
            default: Date.now
        }
    }
);

schema.plugin(mongoosePaginate);

// create model from schema
const UserModel = mongoose.model('UserModel', schema);
export default UserModel;