'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _UsersController = require('./controller/UsersController');

var _UsersController2 = _interopRequireDefault(_UsersController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = new _express.Router();

/*
 * Users API routes
 */
var controller = new _UsersController2.default();
router.get('/users/:id', controller.getUserById);
router.get('/users', controller.getUsers);
router.post('/users', controller.crateNewUser);
router.delete('/users/:id', controller.deleteUser);
router.put('/users/:id', controller.updateUser);

/*
 * Custom routes
 */

/*
 * END of custom routes
 */

exports.default = router;